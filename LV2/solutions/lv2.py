# -*- coding: utf-8 -*-
"""Untitled1.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/15_wpw4_fCmkTmdXG4OJcXm7ZLP2KwaOo
"""

import re
import numpy as np
import random
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
from os import WTERMSIG

f = open("/content/mbox-short.txt", "r")
match = re.findall(r'[\w.+-]+@[\w-]+\.[\w.-]+', f.read())
for i in range(len(match)):
  match[i] = match[i].rsplit("@", 1)[0]

print(match)

sadrzia =[]
for i in range(len(match)):
  if  re.search(r'a', match[i]):
    sadrzia.append(match[i])
print(sadrzia)

sadrzijedana =[]
for i in range(len(match)):
  a=re.findall('a', match[i])
  if  0<len(a)<=1:
    sadrzijedana.append(match[i])
print(sadrzijedana)

nemaa =[]
for i in range(len(match)):
  a=re.findall('a', match[i])
  if  len(a)==0:
    nemaa.append(match[i])
print(nemaa)

malaslova =[]
for i in range(len(match)):
  a=re.findall( '[a-z]+$', match[i])
  if  len(a)>0:
    malaslova.append(match[i])
print(malaslova)

malaslova =[]
for i in range(len(match)):
  a=re.findall( '[0-9]+', match[i])
  if  len(a)>0:
    malaslova.append(match[i])
print(malaslova)

spol=[]
visina=[]
muskarci=[]
zene=[]
brojuzoraka=500
for i in range(0, brojuzoraka):
  x=random.randrange(0,2)
  spol.append(x)
  if x==1:
    y=np.random.normal(180, 7)
    visina.append(y)
    muskarci.append(y)
  if x==0:
    y=np.random.normal(167, 7)
    visina.append(y)
    zene.append(y)

prosjekmuskaraca =np.sum(muskarci)/len(muskarci)
prosjekzena = np.sum(zene)/len(zene)
#print(spol)
#print(visnia)
print(prosjekmuskaraca)
print(prosjekzena)

count, bins, ignored = plt.hist(muskarci, 50, density=True,color='blue')
plt.plot(bins, 1/(7 * np.sqrt(2 * np.pi)) * np.exp( - (bins - 180)**2 / (2 * 7**2) ) , linewidth=2, color='green')
count, bins, ignored = plt.hist(zene, 50, density=True,color='red')
plt.plot(bins, 1/(7 * np.sqrt(2 * np.pi)) * np.exp( - (bins - 170)**2 / (2 * 7**2) ) , linewidth=2, color='green')
plt.plot(prosjekmuskaraca, 0.05, 'o', color='brown');
plt.plot(prosjekzena, 0.05, 'o', color='brown');
plt.show()

x=[]
for i in range(0,100):
  x.append(random.randrange(1,7))
plt.hist(x,6, density=True,color='blue')

with open("/content/mtcars.csv", "r") as f:
  out = []
  for line in f.readlines():
    x = re.findall(r'([^,]+)(?:)', line)
    out.append(x)
b=np.array(out)
mpg=[]
hp=[]
wt=[]
j=0
for i in b:
  if j!=0:
    mpg.append(b[j,1])
    hp.append(b[j,4])
    wt.append(b[j,6])
  j=j+1
plt.stem(hp,mpg)

#Ispišite minimalne, maksimalne i srednje vrijednosti potrošnje automobila. 
plt.stem(hp,wt)
pg=np.array(mpg,float)
print(np.min(pg))
print(np.max(pg))
print(np.sum(pg)/len(pg))

img = mpimg.imread('/content/tiger.png')
svjetlina=img.copy()+0.5
imgplot = plt.imshow(svjetlina)